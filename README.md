# CERN Mattermost on Gitlab/Openshift

## Setting up a temporary instance

This makes sense if e.g. a conference wants to use Mattermost. That way they can use CERN SSO,
local mattermost accounts, or a mix of both (can be configured later).

- *All references to `XXX` should be replaced with a name related to the instance.*
- Create new PaaS/OpenShift project on [OKD](https://paas.cern.ch)
- Create an SSO app with OIDC in the [Applications Portal](https://application-portal.web.cern.ch/)
- Add config for the new instance to the [mm-auth configuration](https://paas.cern.ch/k8s/ns/mm-auth/configmaps/data)
- Add `pg_hba.conf` entry on DBoD for `mmother` user to access the XXX database
- Create database:
```shell
createdb -h dbod-matterm.cern.ch -p 6602 -U admin -O mmother XXX
```
- Create s3 bucket:
```
s3cmd -c ~/.s3cfg.mattermost mb s3://mattermost-XXX
```
- Login to openshift using `oc sso-login paas`
- Select openshift project with `oc project XXX`
- Create openshift serviceaccount:
```shell
oc create serviceaccount gitlabci-deployer
oc policy add-role-to-user registry-editor -z gitlabci-deployer
oc policy add-role-to-user view -z gitlabci-deployer
oc serviceaccounts get-token gitlabci-deployer
```
- Store the token as `OPENSHIFT_TOKEN_PROD_XXX` in the [GitLab variables](https://gitlab.cern.ch/mattermost/docker/-/settings/ci_cd)
- Update `.gitlab-ci.yml` and add a section to deploy to that instance
- The last three steps can be omitted if the instance will never need to be updated with a newer Mattermost version
- Import the image to openshift:
```shell
oc import-image mattermost --namespace XXX --from gitlab-registry.cern.ch/mattermost/docker --confirm
```
- Create a configmap
```shell
oc create configmap settings
oc edit configmap settings
```
and add the following data:
```yaml
data:
  INVITE_SALT: <put random gibberish here>
  LOG_LEVEL: INFO
  OAUTH_API: 'https://mm-auth.web.cern.ch/XXX/api/v4/user'
  OAUTH_AUTHORIZE: 'https://mm-auth.web.cern.ch/XXX/oauth/authorize'
  OAUTH_ID: mattermost-XXX
  OAUTH_SECRET: XXX
  OAUTH_TOKEN: 'https://mm-auth.web.cern.ch/XXX/oauth/token'
  PASSWORD_RESET_SALT: <put random gibberish here>
  PGPASS: <copy from local pgpass>
  PGUSER: mmother
  POSTGRES_DATABASE_NAME: XXX
  POSTGRES_SERVICE_HOST: dbod-matterm.cern.ch
  POSTGRES_SERVICE_PORT: "6603"
  PUBLIC_LINK_SALT: <put random gibberish here>
  S3_ACCESS_KEY: <copy from an existing mattermost instance>
  S3_SECRET_KEY: <copy from an existing mattermost instance>
  S3_BUCKET: mattermost-XXX
  SITE_URL: https://XXX.app.cern.ch
  TZ: Europe/Zurich
```
- Create a new Deployment for the `XXX/mattermost:latest` image stream tag
  - Tick "Auto-deploy when new image is available"
  - Tick "Pause rollouts for this deployment"
  - Set Scaling replica count to 1
- Create persistent volume claim:
  - Storage class: `cephfs`
  - Name: `mattermost-config`
  - Size: 10MB (will be scaled up to 1G anyway)
- Add storage to Deployment:
  - Existing claim: `mattermost-config`
  - Mount Path: `/mattermost/config`
- Add "ALL" environment variables from `settings` configmap to deployment
- Add health checks:
  - Readiness + Liveness + Startup probes
  - Path `/api/v4/system/ping`
  - Timeout: 1 (readiness) / 10 (liveness)
  - Failure threshold (startup probe): 120
- Create a Service:
```yaml
metadata:
  name: mattermost
spec:
  selector:
    app: mattermost
  ports:
    - protocol: TCP
      port: 8080
      targetPort: 8080
```
- Create route:
  - Name: mattermost
  - Hostname: `https://XXX.app.cern.ch`
  - Secure: Yes
  - Insecure Traffic: Redirect
- Resume rollouts
- Login to the new Mattermost instance since the first user becomes a System Admin automatically
- Change visibility to Internet on webservices page
