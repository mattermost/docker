FROM ubuntu:jammy

RUN set -ex && \
	apt-get update && \
	apt-get -y --no-install-recommends upgrade libssl3 && \
	apt-get -y --no-install-recommends install \
		curl \
		ca-certificates \
		openssl \
		jq \
		less \
		tzdata \
		poppler-utils \
		unrtf \
		tidy

RUN set -ex && \
	curl -fsSL 'https://cafiles.cern.ch/cafiles/certificates/CERN%20Root%20Certification%20Authority%202.crt' | openssl x509 -inform DER -out /usr/local/share/ca-certificates/cernroot2.crt && \
	curl -fsSL 'https://cafiles.cern.ch/cafiles/certificates/CERN%20Grid%20Certification%20Authority(1).crt' -o /usr/local/share/ca-certificates/cerngrid.crt && \
	curl -fsSL 'https://cafiles.cern.ch/cafiles/certificates/CERN%20Certification%20Authority.crt' -o /usr/local/share/ca-certificates/cerncert.crt && \
	update-ca-certificates

COPY docker-entry.sh /
COPY mattermost-team-linux-amd64.tar.gz /

RUN set -ex && \
	mkdir -p /mattermost/data /mattermost/plugins /mattermost/client/plugins && \
	tar xzf mattermost-team-linux-amd64.tar.gz && \
	rm mattermost-team-linux-amd64.tar.gz && \
	rm /mattermost/config/config.json && \
	sed -i -e 's/defaultMessage:"GitLab"/defaultMessage:"Login"/g' /mattermost/client/*.js && \
	sed -i -e 's/defaultMessage:"GitLab Single Sign-On"/defaultMessage:"CERN Single Sign-On"/g' /mattermost/client/*.js && \
	sed -i -e 's/"login.gitlab":"GitLab"/"login.gitlab":"Login"/g' /mattermost/client/*.js && \
	sed -i -e 's/"signup.gitlab":"GitLab Single Sign-On"/"signup.gitlab":"CERN Single Sign-On"/g' /mattermost/client/*.js && \
	sed -i -e 's/"login.gitlab": "GitLab"/"login.gitlab": "Login"/g' /mattermost/client/i18n/*.json && \
	sed -i -e 's/"signup.gitlab": "GitLab Single Sign-On"/"signup.gitlab": "CERN Single Sign-On"/g' /mattermost/client/i18n/*.json && \
	chmod -R a+w /mattermost

ENTRYPOINT ["/docker-entry.sh"]
CMD ["mattermost"]

EXPOSE 8080
