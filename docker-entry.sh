#!/bin/bash

set -e

config=/mattermost/config/config.json

function confstr() {
	local tmp="$(mktemp)"
	local name="$1"
	local value="$2"
	if [[ -z $value ]]; then
		echo "no value provided for $name"
		exit 1
	fi
	jq --arg VAL "$value" "$name = \$VAL" "$config" > "$tmp" && mv "$tmp" "$config"
}

function confjson() {
	local tmp="$(mktemp)"
	local name="$1"
	local value="$2"
	if [[ -z $value ]]; then
		echo "no value provided for $name"
		exit 1
	fi
	jq --argjson VAL "$value" "$name = \$VAL" "$config" > "$tmp" && mv "$tmp" "$config"
}

function conffile() {
	local tmp="$(mktemp)"
	local name="$1"
	local file="$2"
	jq --slurpfile VAL "$file" "$name = \$VAL[]" "$config" > "$tmp" && mv "$tmp" "$config"
}

if [[ -z $SITE_URL ]]; then
	echo 'config env vars missing - check config map!'
	exit 1
fi

if [[ -z $POSTGRES_DATASOURCE ]]; then
	encpgpass=$(printf %s "$PGPASS" | jq -s -R -r @uri)
	datasource="postgres://$PGUSER:$encpgpass@$POSTGRES_SERVICE_HOST:$POSTGRES_SERVICE_PORT/$POSTGRES_DATABASE_NAME?sslmode=require&connect_timeout=10"
else
	datasource="$POSTGRES_DATASOURCE"
fi

if [[ ! -f $config ]]; then

	echo '{}' > $config
	confstr  '.ServiceSettings.SiteURL' "$SITE_URL"
	confstr  '.ServiceSettings.ListenAddress' ':8080'
	confjson '.ServiceSettings.EnableOAuthServiceProvider' 'true'
	confjson '.ServiceSettings.EnableIncomingWebhooks' 'true'
	confjson '.ServiceSettings.EnableOutgoingWebhooks' 'true'
	confjson '.ServiceSettings.EnableCommands' 'true'
	confjson '.ServiceSettings.EnableOnlyAdminIntegrations' 'false'
	confjson '.ServiceSettings.EnablePostUsernameOverride' 'true'
	confjson '.ServiceSettings.EnablePostIconOverride' 'true'
	confjson '.ServiceSettings.EnableLinkPreviews' 'true'
	confjson '.ServiceSettings.EnableSecurityFixAlert' 'false'
	confjson '.ServiceSettings.EnableUserAccessTokens' 'true'
	confjson '.ServiceSettings.EnableCustomEmoji' 'true'
	confjson '.ServiceSettings.EnableGifPicker' 'true'
	confstr  '.ServiceSettings.ExperimentalGroupUnreadChannels' 'default_off'
	confstr  '.ServiceSettings.ExperimentalChannelSidebarOrganization' 'default_off'

	confjson '.DisplaySettings.ExperimentalTimezone' 'true'

	confstr  '.TeamSettings.SiteName' 'CERN Mattermost'
	confjson '.TeamSettings.MaxUsersPerTeam' '10000'
	confstr  '.TeamSettings.CustomDescriptionText' 'Stay in touch with your colleagues.'
	confstr  '.TeamSettings.TeammateNameDisplay' 'full_name'

	confjson '.SqlSettings.MaxIdleConns' '10'
	confjson '.SqlSettings.MaxOpenConns' '10'
	confjson '.SqlSettings.QueryTimeout' '90'
	confstr  '.SqlSettings.DriverName' 'postgres'
	confstr  '.SqlSettings.DataSource' "$datasource"
	if [[ ! -z $AT_REST_ENCRYPT_KEY ]]; then
		confstr  '.SqlSettings.AtRestEncryptKey' "$AT_REST_ENCRYPT_KEY"
	fi

	confjson '.LogSettings.EnableConsole' 'true'
	confstr  '.LogSettings.ConsoleLevel' "$LOG_LEVEL"
	confjson '.LogSettings.ConsoleJson' 'false'
	confjson '.LogSettings.EnableFile' 'true'
	confstr  '.LogSettings.FileLevel' "$LOG_LEVEL"
	confjson '.LogSettings.FileJson' 'false'
	confjson '.LogSettings.EnableDiagnostics' 'false'

	confjson '.NotificationLogSettings.EnableConsole' 'true'
	confstr  '.NotificationLogSettings.ConsoleLevel' 'ERROR'
	confjson '.NotificationLogSettings.ConsoleJson' 'false'
	confjson '.NotificationLogSettings.EnableFile' 'true'
	confstr  '.NotificationLogSettings.FileLevel' 'ERROR'
	confjson '.NotificationLogSettings.FileJson' 'false'

	confjson '.FileSettings.EnablePublicLink' 'true'
	confstr  '.FileSettings.PublicLinkSalt' "$PUBLIC_LINK_SALT"
	confstr  '.FileSettings.DriverName' 'amazons3'
	confstr  '.FileSettings.AmazonS3AccessKeyId' "$S3_ACCESS_KEY"
	confstr  '.FileSettings.AmazonS3SecretAccessKey' "$S3_SECRET_KEY"
	confstr  '.FileSettings.AmazonS3Bucket' "$S3_BUCKET"
	confstr  '.FileSettings.AmazonS3Endpoint' 's3.cern.ch'
	confjson '.FileSettings.AmazonS3SSL' 'true'

	confjson '.EmailSettings.SendEmailNotifications' 'true'
	confstr  '.EmailSettings.FeedbackName' 'Mattermost Notification'
	confstr  '.EmailSettings.FeedbackEmail' 'mattermost-noreply@cern.ch'
	confstr  '.EmailSettings.ReplyToAddress' 'mattermost-noreply@cern.ch'
	confstr  '.EmailSettings.SMTPServer' 'cernmx.cern.ch'
	confstr  '.EmailSettings.SMTPPort' '25'
	confjson '.EmailSettings.SendPushNotifications' 'true'
	confstr  '.EmailSettings.PushNotificationServer' 'https://push-test.mattermost.com'
	confstr  '.EmailSettings.PushNotificationContents' 'generic'
	confstr  '.EmailSettings.InviteSalt' "$INVITE_SALT"
	confstr  '.EmailSettings.PasswordResetSalt' "$PASSWORD_RESET_SALT"
	confjson '.EmailSettings.EnableSignUpWithEmail' 'false'
	confjson '.EmailSettings.EnableSignInWithEmail' 'false'
	confjson '.EmailSettings.EnableSignInWithUsername' 'false'

	confjson '.PrivacySettings.ShowEmailAddress' 'true'
	confjson '.PrivacySettings.ShowFullName' 'true'

	confstr  '.SupportSettings.ReportAProblemLink' 'https://cern.service-now.com/service-portal/function.do?name=mattermost'
	confstr  '.SupportSettings.SupportEmail' 'service-desk@cern.ch'
	confjson '.SupportSettings.EnableAskCommunityLink' 'false'

	confjson '.AnnouncementSettings.AdminNoticesEnabled' 'false'

	confjson '.GitLabSettings.Enable' 'true'
	confstr  '.GitLabSettings.Secret' "$OAUTH_SECRET"
	confstr  '.GitLabSettings.Id' "$OAUTH_ID"
	confstr  '.GitLabSettings.AuthEndpoint' "$OAUTH_AUTHORIZE"
	confstr  '.GitLabSettings.TokenEndpoint' "$OAUTH_TOKEN"
	confstr  '.GitLabSettings.UserApiEndpoint' "$OAUTH_API"

	confjson '.AnalyticsSettings.MaxUsersForStatistics' '15000'

	confjson '.PluginSettings.EnableMarketplace' 'true'
	confjson '.PluginSettings.EnableUploads' 'true'
	confjson '.PluginSettings.PluginStates.jira.Enable' 'false'
	confjson '.PluginSettings.PluginStates."com.mattermost.nps".Enable' 'false'
	confjson '.PluginSettings.PluginStates."com.github.matterpoll.matterpoll".Enable' 'true'
	confjson '.PluginSettings.PluginStates."com.github.scottleedavis.mattermost-plugin-remind".Enable' 'true'

	echo 'Created config file'
else
	echo 'Config file exists'

	# we can't change these settings at runtime, so we always sync them
	confstr '.SqlSettings.DataSource' "$datasource"
	confstr '.ServiceSettings.SiteURL' "$SITE_URL"
fi

# we keep this mostly because the autolink config is MUCH more pleasant with a decent editor
# at some point we could remove all the other plugins from here and just configure them via the
# web interface...
echo 'Applying plugin settings'
if [[ -f /mattermost/configdata/autolink.json ]]; then
	echo '- autolink'
	confjson '.PluginSettings.PluginStates."mattermost-autolink".Enable' 'true'
	conffile '.PluginSettings.Plugins."mattermost-autolink".links' /mattermost/configdata/autolink.json
fi
if [[ -f /mattermost/configdata/github.json ]]; then
	echo '- github'
	confjson '.PluginSettings.PluginStates.github.Enable' 'true'
	conffile '.PluginSettings.Plugins.github' /mattermost/configdata/github.json
fi
if [[ -f /mattermost/configdata/gitlab.json ]]; then
	echo '- gitlab'
	confjson '.PluginSettings.PluginStates."com.github.manland.mattermost-plugin-gitlab".Enable' 'true'
	conffile '.PluginSettings.Plugins."com.github.manland.mattermost-plugin-gitlab"' /mattermost/configdata/gitlab.json
fi
if [[ -f /mattermost/configdata/cern.json ]]; then
	echo '- cern'
	confjson '.PluginSettings.PluginStates.cern.Enable' 'true'
	conffile '.PluginSettings.Plugins.cern' /mattermost/configdata/cern.json
fi
if [[ -f /mattermost/configdata/zoom.json ]]; then
	echo '- zoom'
	confjson '.PluginSettings.PluginStates.zoom.Enable' 'true'
	conffile '.PluginSettings.Plugins.zoom' /mattermost/configdata/zoom.json
fi
if [[ -f /mattermost/configdata/standupraven.json ]]; then
	echo '- standup raven'
	confjson '.PluginSettings.PluginStates."standup-raven".Enable' 'true'
	conffile '.PluginSettings.Plugins."standup-raven"' /mattermost/configdata/standupraven.json
fi

cd /mattermost
export PATH="$PATH:/mattermost/bin"

if [[ "$1" = 'mattermost' ]]; then
	echo "Starting mattermost server"
	exec mattermost
fi

exec "$@"
